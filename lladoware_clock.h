// lladoware_timing.c
//
// Timing functions for TI TM4C123GXL using Keil v5
// Clock that records time since startup using interrupts
//
// This file is part of lladoware v1.1
// Travis Llado, travis@travisllado.com
// Last modified 2021-02-21

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include "lladoware_system_config.h"

////////////////////////////////////////////////////////////////////////////////
// Constants

#define TICK_FREQUENCY  1000  // Hz
#define TICK_PRIORITY   1
#define TICK_PERIOD     SYS_FREQ/TICK_FREQUENCY

////////////////////////////////////////////////////////////////////////////////
// External Prototypes

struct clock_type{
    uint32_t hr;
    uint32_t min;
    uint32_t sec;
};

// clock_init()
// Initializes clock that records time since startup
void clock_init(void);

// clock_add()
// Add operator for clock_type
struct clock_type clock_add(const struct clock_type lhs, const struct clock_type rhs);

// clock_equal_to()
// Equal-to operator for clock_type
uint32_t clock_equal_to(const struct clock_type lhs, const struct clock_type rhs);

// clock_ms()
// Returns milliseconds since last second tick
uint32_t clock_ms(void);

// clock_sec()
// Returns seconds since startup
uint32_t clock_sec(void);

// clock_time()
// Returns current time in struct clock_type format
struct clock_type clock_time(void);

////////////////////////////////////////////////////////////////////////////////
// End of file
