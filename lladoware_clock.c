// lladoware_timing.c
//
// Timing functions for TI TM4C123GXL using Keil v5
// Clock that records time since startup using interrupts
//
// This file is part of lladoware v1.1
// Travis Llado, travis@travisllado.com
// Last modified 2021-02-27

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "tm4c123gh6pm.h"
#include "lladoware_clock.h"

void DisableInterrupts(void);
void EnableInterrupts(void);

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void Timer1_Init(void);
void Timer1A_Handler(void);

////////////////////////////////////////////////////////////////////////////////
// Global Variables

uint32_t clock_value_ms = 0;
uint32_t clock_value_sec = 0;

////////////////////////////////////////////////////////////////////////////////
// clock_add()
// Add operator for clock_type

struct clock_type clock_add(const struct clock_type lhs, const struct clock_type rhs) {
  struct clock_type output = {0, 0, 0};

  output.sec = lhs.sec + rhs.sec;

  if(output.sec >= 60) {
    output.min = 1;
    output.sec -= 60;
  }

  output.min += lhs.min + rhs.min;

  if(output.min >= 60) {
    output.hr = 1;
    output.min -= 60;
  }

  output.hr += lhs.hr + rhs.hr;

  return output;
}

////////////////////////////////////////////////////////////////////////////////
// clock_equal_to()
// Equal-to operator for clock_type

uint32_t clock_equal_to(const struct clock_type lhs, const struct clock_type rhs) {
  return (lhs.hr == rhs.hr) && (lhs.min == rhs.min) && (lhs.sec == rhs.sec);
}

////////////////////////////////////////////////////////////////////////////////
// clock_init()
// Initializes clock that records time since startup

void clock_init(void) {
  Timer1_Init();
}

////////////////////////////////////////////////////////////////////////////////
// clock_ms()
// Returns milliseconds since last second tick

uint32_t clock_ms(void) {
  return clock_value_ms;
}

////////////////////////////////////////////////////////////////////////////////
// clock_sec()
// Returns seconds since startup

uint32_t clock_sec(void) {
  return clock_value_sec;
}

////////////////////////////////////////////////////////////////////////////////
// clock_time()
// Returns current time in struct clock_type format

struct clock_type clock_time(void) {
  const uint32_t time_now = clock_sec();
  struct clock_type clock_time;
  clock_time.hr = time_now / 3600;
  clock_time.min = (time_now % 3600) / 60;
  clock_time.sec = time_now % 60;

  return clock_time;
}

////////////////////////////////////////////////////////////////////////////////
// Timer1_Init()
// Initializes Timer1A for clock

void Timer1_Init(void) {
  DisableInterrupts();
    SYSCTL_RCGCTIMER_R |= SYSCTL_RCGCTIMER_R1;                        // activate TIMER1
    while((SYSCTL_PRTIMER_R & SYSCTL_PRTIMER_R1) == 0) {}             // wait for timer to start
    TIMER1_CTL_R = 0x00;                                              // disable TIMER1A during setup
    TIMER1_CFG_R = 0x00;                                              // set to 32-bit mode
    TIMER1_TAMR_R = 0x02;                                             // set to periodic mode
    TIMER1_TAILR_R = TICK_PERIOD - 1;                                 // set reset value
    TIMER1_TAPR_R = 0;                                                // set bus clock resolution
    TIMER1_ICR_R = 0x01;                                              // clear TIMER1A timeout flag
    TIMER1_IMR_R |= 0x01;                                             // arm timeout interrupt
    NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (TICK_PRIORITY << 29); // set priority
    NVIC_EN0_R = 1 << 21;                                             // enable IRQ 21 in NVIC
    TIMER1_CTL_R = 0x01;                                              // activate TIMER1A
  EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// Timer1A_Handler()
// Time updater, called by timer interrupt

void Timer1A_Handler(void) {
  TIMER1_ICR_R = 0x01;  // acknowledge timer1A timeout

  clock_value_ms++;
  if(clock_value_ms == 1000){
    clock_value_ms = 0;
    clock_value_sec++;
  }
}

////////////////////////////////////////////////////////////////////////////////
// End of file
